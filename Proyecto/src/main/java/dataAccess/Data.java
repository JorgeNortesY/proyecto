package dataAccess;

import java.util.ArrayList;
import java.util.HashMap;

import models.Address;
import models.Identificable;
import models.Mail;
import models.Nif;
import models.Password;
import models.Session;
import models.Simulation;
import models.User;
import models.User.RoleUser;
import utils.EasyDate;

public class Data {

	private ArrayList<Identificable> usersData;
	private ArrayList<Identificable> sessionsData;
	private ArrayList<Identificable> simulationsData;
	private HashMap<String,String> idEquivalente;

	public Data() {		
		this.usersData = new ArrayList<Identificable>();
		this.sessionsData = new ArrayList<Identificable>();
		this.simulationsData = new ArrayList<Identificable>();
		this.idEquivalente = new HashMap<String,String>();
		loadIntegratedUsers();
	}

	private void loadIntegratedUsers() {
		this.createUser(new User(new Nif("00000000T"),
				"Admin",
				"Admin Admin",
				new Address("La Iglesia", "0", "30012", "Patiño"),
				new Mail("admin@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
		this.createUser(new User(new Nif("00000001R"),
				"Guest",
				"Guest Guest",
				new Address ("La Iglesia", "0", "30012", "Patiño"),
				new Mail("guest@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
		
	}

	// Users

	public User findUser(String id) {
		
		id = this.idEquivalente.get(id); 
		int pos = this.indexSort(usersData, id);
	

		if (pos >= 0) {
			return (User) this.usersData.get(pos);
		}
		return null;
	}
	

	public void createUser(User user) {

//		if (findUser(user.getNif().getText()) == null) {
//			this.usersData.add(user);
//			this.idEquivalente.put(user.getNif().getText(), user.getId());
//			this.idEquivalente.put(user.getMail().getTexto(), user.getId());
//
//		}
		int index = this.indexSort(this.usersData, user.getId());
		if (index < 0) {
			this.usersData.add((index+1), user);
			this.idEquivalente.put(user.getNif().getText(), user.getId());
			this.idEquivalente.put(user.getMail().getTexto(), user.getId());
		}
	}



	private int indexSort(ArrayList<Identificable> usersData, String id) {
		 int inicio = 0;
		 int fin = this.usersData.size() - 1;
		 int pos;
		 while (inicio <= fin) {
		     pos = (inicio + fin) / 2;
		     if (this.usersData.get(pos).getId().equals(id)) {
		    	 return pos;
		     }
		     else { 
		    	 if (this.usersData.get(pos).getId().compareTo(id) < 0) {
		    		 inicio = pos + 1;
		    	 } else {
		    		 fin = pos - 1;
		     	}
		     }
		 }
		 return -1;
	}

	public void updateUser(User user) {
		User userOld = findUser(user.getNif().getText());
		if (userOld != null) {
			
			this.usersData.set(this.indexOfUser(userOld), user);
			this.idEquivalente.replace(userOld.getNif().getText(), user.getId());
			this.idEquivalente.remove(userOld.getMail().getTexto(), user.getId());
		}
	}

	public void deleteUser(String id) {
		User user = findUser(id);

		if (user != null) {
			this.usersData.remove(this.indexOfUser(user));
			this.idEquivalente.remove(user.getNif().getText());
			this.idEquivalente.remove(user.getMail().getTexto());
		}
	}

	private int indexOfUser(User user) {
		for (int i=0; i < this.usersData.size(); i++) {
			if (user.equals(this.usersData.get(i))) {
				return i;
			}
		}
		return -1;
	}

	// Sessions

	public Session findSession(String id) {
		int pos = this.binaryFindSession(id);
		if (pos >= 0) {
			return (Session) this.sessionsData.get(pos);
		}
		return null;	
	}
	
	private int binaryFindSession(String id) {
		 int inicio = 0;
		 int fin = this.sessionsData.size() - 1;
		 int pos;
		 while (inicio <= fin) {
		     pos = (inicio + fin) / 2;
		     if (this.sessionsData.get(pos).getId().equals(id)) {
		    	 return pos;
		     }
		     else { 
		    	 if (this.sessionsData.get(pos).getId().compareTo(id) < 0) {
		    		 inicio = pos + 1;
		    	 } else {
		    		 fin = pos - 1;
		     	}
		     }
		 }
		 return -1;
   }

	public void createSession(Session session) {
		this.sessionsData.add(session);
	}

	public void updateSession(Session session) {
		Session sessionOld = this.findSession(session.getId());
		if (sessionOld != null) {
			this.sessionsData.set(this.indexOfSession(sessionOld), session);
			return;
		}
	}

	private int indexOfSession(Session session) {
		for (int i=0; i < this.sessionsData.size(); i++) {
			if (session.equals(this.sessionsData.get(i))) {
				return i;
			}
		}
		return -1;
	}
	
	// Simulations

	public Simulation findSimulation(String id) {
		int pos = this.binaryFindSimulation(id);
		if (pos >= 0) {
			return (Simulation) this.simulationsData.get(pos);
		}
		return null;
	}
	
	private int binaryFindSimulation(String id) {
		 int inicio = 0;
		 int fin = this.simulationsData.size() - 1;
		 int pos;
		 while (inicio <= fin) {
		     pos = (inicio + fin) / 2;
		     if (this.simulationsData.get(pos).getId().equals(id)) {
		    	 return pos;
		     }
		     else { 
		    	 if (this.simulationsData.get(pos).getId().compareTo(id) < 0) {
		    		 inicio = pos + 1;
		    	 } else {
		    		 fin = pos - 1;
		     	}
		     }
		 }
		 return -1;
  }

	public void createSimulation(Simulation simulation) {
		if (findUser(simulation.getId()) == null) {
			this.simulationsData.add(simulation);
		}
	}
	
	public void updateSimulation(Simulation simulation) {
		Simulation simulationOld = this.findSimulation(simulation.getId());
		if (simulationOld != null) {
			this.simulationsData.set(this.indexOfSimulation(simulationOld), simulation);
		}
	}

	private int indexOfSimulation(Simulation simulation) {
		for (int i=0; i < this.simulationsData.size(); i++) {
			if (simulation.equals(this.simulationsData.get(i))) {
				return i;
			}
		}
		return -1;
	}

}
