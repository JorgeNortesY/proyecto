package models;

import utils.EasyDate;

public class User extends Person implements Identificable {

	public enum RoleUser { GUEST, REGISTERED, ADMIN };
	
	private EasyDate registeredDate;
	private Password password;
	private RoleUser role;

	public User(Nif nif, String name, String surnames,
			Address address, Mail mail, EasyDate birthDate,
			EasyDate registeredDate, Password password, RoleUser role) {
		super(nif, name, surnames, address, mail, birthDate);
		this.setRegisteredDate(registeredDate);
		this.setPassword(password);
		this.setRole(role);
	}

	public User() {
		super();
		this.registeredDate = EasyDate.today();	
		this.password = new Password("Miau#00");
		this.role = RoleUser.REGISTERED;
	}

	public User(User user) {
		assert user != null;
		this.nif = new Nif(user.nif);
		this.name = new String(user.name);
		this.surnames = new String(user.surnames);
		this.address = new Address(user.address);
		this.mail = new Mail(user.mail);	
		this.birthDate = user.birthDate.clone();
		this.registeredDate = user.registeredDate.clone();
		this.password = new Password(user.password);
		this.role = RoleUser.REGISTERED;	
	}
	
	@Override
	public String getId() {
		return this.nif.getText();
	}

	

	public EasyDate getRegisteredDate() {
		return this.registeredDate;
	}

	public Password getPassword() {
		return this.password;
	}

	public RoleUser getRole() {
		return this.role;
	}

	
	public void setRegisteredDate(EasyDate registeredDate) {
		assert registeredDate != null;

		if (isValidRegisteredDate(registeredDate)) {
			this.registeredDate = registeredDate;
		}
	}

	private boolean isValidRegisteredDate(EasyDate registeredDate) {	
		return !registeredDate.isAfter(EasyDate.now());
	}

	public void setPassword(Password password) {
		assert password != null;
		this.password = password;

	}

	public void setRole(RoleUser role) {
		this.role = role;
	}

	@Override
	public User clone() {
		return  new User(this);
	}

	@Override
	public String toString() {
		return super.toString() + String.format(
				"%15s %-15s\n"
						+ "%15s %-15s\n"
						+ "%15s %-15s\n"
						+"registeredDate:", registeredDate, 
						"password:", password, 
						"role:", role
				);
	}


	public int compareTo(User user) {
		return this.getId().compareTo(user.getId());
	}

}